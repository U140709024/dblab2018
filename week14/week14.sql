Create table `employees_backup` (

	`id` int auto_increment,
    `EmployeeID` int(11) not null,
    `LastName` varchar(45) not null,
    `FirstName` varchar(45) not null,
    `BirthDate` varchar(45) not null,
    `changedat` datetime default null,
    `action` varchar(50) default null,
    primary key (`id`)
);

SELECT * FROM employees_backup;

update employees
set LastName= "Abak"
where EmployeeID = 6;

update employees
set BirthDate= "1999-01-01"
where EmployeeID = 8;

insert into employees(LastName,FirstName,BirthDate,Photo,Notes)
         values("Abak","Başak","1996-07-02","EmpID11.pic","Intern");
         
delete from  employees
where EmployeeID =11;        

