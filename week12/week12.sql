alter table employees
add country varchar(100);

alter table employees
add bdate date;

alter table employees
modify column bdate year;

alter table employees
drop column country;

truncate table orderdetails;