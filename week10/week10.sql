SELECT * FROM company.customers;

#indexing 

explain customers;

select * from customers
order by CustomerName;

select * from customers
order by CustomerID;

create index abc on customers(CustomerName);

explain customers;

select * from customers
order by CustomerName;

#create view

create view myview as 
select * from customers
order by CustomerName;

select * from myview;

insert into myview 
values(10000,"a","a","a","a","a","a");

#create view2
create view myview2 as 
select customers.CustomerID as c1,orders.CustomerID as c2 
from  customers,orders
where customers.CustomerID = orders.CustomerID;