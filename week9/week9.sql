select count(customers.CustomerID)  as numberOfCustomers,customers.Country
from customers
group by customers.Country
order by numberOfCustomers desc;

select count(products.ProductID) as numberOfProducts , suppliers.SupplierName 
from products join suppliers on products.SupplierID = suppliers.SupplierID
group by products.SupplierID
order by numberOfProducts desc;

load data  local infile "C:\\Users\\Basak\\Desktop\\Database\\dblab2018\\week9\\customers.csv" 
into table customers
fields terminated by ';'
ignore 1 lines;  

load data  local infile "C:\\Users\\Basak\\Desktop\\Database\\dblab2018\\week9\\employees.csv" 
into table employees
fields terminated by ','
ignore 1 lines;  

load data  local infile "C:\\Users\\Basak\\Desktop\\Database\\dblab2018\\week9\\categories.tsv" 
into table categories;

select * from customers;

#delete from customers;

select * from employees;

select * from categories;
